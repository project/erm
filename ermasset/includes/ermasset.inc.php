<?php

function ermasset_getheaders($option) {
  switch ($option) {
    case 'assets':
      /* ZZZZ until the new static data module is completed it's a pain to display the status (since it's just in an array
      not in the database so I can't make a view. For now we're taking status out. We can't show type either */
      $header = array(t('ERM'), t('Name'), t('Model'), t('Serial'), t('Contact'));
    break;
  }

  return $header;

} // function ermasset_getheaders


function ermasset_getquery($option, $form_values, $rowid = NULL) {

  /* this function is called by the submit of the search form 
  the option will either be 'asset' or 'assets', returning either a single row or a collection of rows */

/* the initial code didn't check whether or not the user had entered values in any of the fields so the SELECT statement was always complex I've rewritten it to simplify the queries */
  $where = "WHERE";		
  $count = 0;

  switch ($option) {
    case 'assets':
        $query = "SELECT A.id, A.name, A.adesc, A.serialno, A.model, A.astid, A.user_id, U.name AS uname FROM erm_asset A LEFT JOIN " .
        "users U on U.uid = A.user_id ";

      if (strlen($form_values['ermasset_id']) > 0) {
        $where .= " (A.id like '%".$form_values['ermasset_id']."%')";
        $count++;
      }
      if (strlen($form_values['ermasset_serialno']) > 0) {
        if ($count > 0) $where .= " AND";
          $where .= " (A.serialno like '%".$form_values['ermasset_serialno']."%') ";
    }	
      if (strlen($form_values['ermasset_ponumber']) > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (A.ponumber like '%".$form_values['ermasset_ponumber']."%') ";
    }	
      if (strlen($form_values['ermasset_acctinvnum']) > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (A.acctinvnum like '%".$form_values['ermasset_acctinvnum']."%') ";
    }	
     if ($form_values['ermasset_type'] > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (A.typeflag = " . $form_values['ermasset_type'] . ") ";
    }	
     if ($form_values['ermasset_jid'] > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (A.prjid = " . $form_values['ermasset_jid'] . ") ";
    }	
     if ($form_values['ermasset_userid'] > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (A.user_id = " . $form_values['ermasset_userid'] . ") ";
    }	
      if (strlen($form_values['ermasset_ip']) > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (A.ipaddr like '%".$form_values['ermasset_ip']."%') ";
    }
    /* with location fields we need only check the lowest one they choose */
    if ($form_values['roomid'] > 0) {	
        if ($count > 0) $where .= " AND";
        $where .= " (A.roomid = " . $form_values['bar-4'] . ") ";
    }
    else if ($form_values['buildingid'] > 0) {	
        if ($count > 0) $where .= " AND";
        $where .= " (A.buildingid = " . $form_values['bar-3'] . ") ";
    }
    else if ($form_values['townid'] > 0) {	
        if ($count > 0) $where .= " AND";
        $where .= " (A.townid = " . $form_values['bar-2'] . ") ";
    }  
    else if ($form_values['countryid'] > 0) {	
        if ($count > 0) $where .= " AND";
        $where .= " (A.countryid = " . $form_values['bar-1'] . ") ";
    }

       break;
   
    case 'asset':
      $query = "SELECT * " .
               "FROM {erm_asset} A " .
               "WHERE id = '".$rowid."' ";
    break;
  }
  // ZZZZ dunno what this does????
  if (strlen($where) > 6) $query .= $where;
  return $query;

} // function ermasset_erm_query;

/* formats a single row (with a link for the ERM) and the ERM ID prefixed by the letters "ERM" */
function ermasset_genrows($option, $links) {
  switch ($option) {
    case 'assets':
      $row = array('ERM' => '<a href="'.arg(1).'/'.$links->id.'">ERM'.$links->id.'</a>', 'Name' => $links->name, 'Model' => $links->model, 'Serial' => $links->serialno, 'Contact' => $links->uname);
    break;
  }

  return $row;

} //function ermasset_genrows


function ermasset_gettabledata($option, $form_values) {

  $output = '';

  $rows = array();

  // set display type depending on users choice
  $header = ermasset_getheaders($option);
  $query = ermasset_getquery($option, $form_values);

  // get data from table
  $queryResult = db_query($query);

  $count = 0;
  while ($links = db_fetch_object($queryResult)) {
    $rows[] = ermasset_genrows($option, $links); 
    $count++;
  }

  $table .= theme('table', $header, $rows ? $rows : array(array(array('data' => t('No data was returned.'), 'colspan' => 10))));
  if ($count > 1) $output .= '<p>'.$count.' '.t('records were returned.');

  $output .= theme('box', check_plain($title), $table); 
  return $output;
} // function ermasset_gettabledata

function erm_asset_formatrecord($option, $rowid) {

  switch ($option) {
    case 'assets':
      $header = array('key' => array('data' => t('ERM').arg(2), 'colspan' => 2));
      $title = 'Viewing Asset';
    case 'asset':
      $header = array('key' => array('data' => t('ERM').arg(2), 'colspan' => 2));
      $title = 'Viewing Asset';
    break;
  }

  $links = ermasset_getrowdata($option, $rowid);
  $rows = array();

   /* lets make an array of perm options */
  $perms = array('1','2','6');

   /* retrieve information on this table from the static_data table */
  $fieldlist = sGetTable('erm', 'erm_asset');

  while ($sdata = db_fetch_object($fieldlist)) {
    //we have to find the matching value in the $links array
    $keystring = t('$val = $links->' . $sdata->snamekey . ";");
    eval($keystring);
    /* we now know the value of the field, a case statment now to deal with the static lists, check boxes etc */
    switch ($sdata->stype) {
      case '0':
      case '1':
      case '2':
        //textboxes & textareas & dates
        $rows[] = array('key' => $sdata->svalue, 'val' => $val);
        break;
      case '3':
      case '4':
      case '5':
        // the default select/checkbox/radiobutton statement, call a function with the modulename, listname and value
        $newval = sGetListValue('ermasset', $sdata->slistname, $val);
        $rows[] = array('key' => $sdata->svalue, 'val' => $newval);
        break;
      case '7':
        break;    // don't do anything with fieldsets
      case '10':
        // specialised select statement - get the parent asset
        if ($val > 0) {
          $query = "SELECT id, name FROM {erm_asset} WHERE id = '" . $val . "'";
          $queryResult = db_query($query);
          $assetrecord = db_fetch_object($queryResult);
          $newval = $assetrecord->id . " | " . $assetrecord->name;
        }
        else {
          $newval = "";
        }
        $rows[] = array('key' => $sdata->svalue, 'val' => $newval);
        break;
      case '11':
        // specialised select statement - get the project
        if ($val > 0) {
          $query = "SELECT jid, jname FROM {erm_profit_jobs} WHERE jid = '" . $val . "'";
          $queryResult = db_query($query);
          $projectrecord = db_fetch_object($queryResult);
          $newval = $projectrecord->jname;
        }
        else {
          $newval = "";
        }
        $rows[] = array('key' => $sdata->svalue, 'val' => $newval);
        break;
      case '12':
        // specialised select statement - get the owner
         if ($val > 0) {
          $query = "SELECT uid, name FROM {users} WHERE uid = '" . $val . "'";
          $queryResult = db_query($query);
          $userrecord = db_fetch_object($queryResult);
          $newval = $userrecord->name;
        }
        else {
          $newval = "";
        }
        $rows[] = array('key' => $sdata->svalue, 'val' => $newval);
        break;
      case '13':
        // specialised select statement - get the supplier
        if ($val > 0) {
          $query = "SELECT supid, name FROM {erm_supplier} WHERE supid = '" . $val . "'";
          $queryResult = db_query($query);
          $supplierrecord = db_fetch_object($queryResult);
          $newval = $supplierrecord->name;
        }
        else {
          $newval = "";
        }
        $rows[] = array('key' => $sdata->svalue, 'val' => $newval);
        break;
      case '14':
        if ($val > 0) {
          $query = "SELECT loc_id, description FROM {erm_location} WHERE loc_id = '" . $val . "'";
          $queryResult = db_query($query);
          $locationrecord = db_fetch_object($queryResult);
          $newval = $locationrecord->description;
        }
        else {
          $newval = "";
        }
 
        $rows[] = array('key' => $sdata->svalue, 'val' => $newval);

        break;
     default:
        $rows[] = array('key' => $sdata->svalue, 'val' => $val);
    }
  }

  $table .= theme('table', $header, $rows ? $rows : array(array(array('data' => t('No data was returned.'), 'colspan' => 2))));
  $output .= theme('box', check_plain($title), $table);

  return $output;
} //erm_asset_formatrecord



function ermasset_getoptions($option, $sort = 'none', $key, $optval) {

  $output = array();
  // build the query for this request
  $query = ermasset_getquery($option, $sort, $rowid = NULL);

  // get data from table
  $queryResult = db_query($query);

  while ($links = db_fetch_object($queryResult)) {
    $select[$links->$key] = $links->$optval;
  }

  return $select;
} // function ermasset_getoptions


function ermasset_getrowdata($option, $rowid) {

  // build the query for this request
  $query = ermasset_getquery($option, $sort = 'none', $rowid);

  // get data from table
  $queryResult = db_query($query);

  if (!$links = db_fetch_object($queryResult)) {
    $errormessage = 'No record with id of '.$rowid.' was found for '.$option;
    drupal_set_message($errormessage, error);
  }

  return $links;
} // function ermasset_getrowdata

// a simple function which returns an arrag of key value pairs where the key is the fieldname and the value
// is the current value
function ermasset_getasset($rowid) {
  $query = t("SELECT * FROM {erm_asset} WHERE id = '" . $rowid . "'");

  // get data from table
  $queryResult = db_query($query);
  // retrieves a single row
  if (!$links = db_fetch_object($queryResult)) {
    $errormessage = "No record with id of '.$rowid.' was found";
    drupal_set_message($errormessage, error);
  }
  return $links;
}


function ermasset_delete_confirm($option, $rowid) {
  $form['groups']['id'] = array(
    '#type' => 'item',
    '#title' => t(''),
    '#value' => 'Are you sure you wish to delete this entry?'
  );

  $form['formname'] = array(
    '#type' => 'hidden',
    '#title' => t('Form name'),
    '#value' => $option
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#title' => t('Id'),
    '#value' => $rowid
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel'
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#submit' => TRUE,
    '#value' => 'Delete'
  );

  return $form;

} //function ermasset_delete_confirm
