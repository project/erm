<?php

/* note that the previous naming convention - putting addnew everywhere - has been abandoned. This is because the same
form will probably be used for view and edit */


function it_form() {
  $form['computing'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => 'IT Related Fields',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  /* the name field from the exiting table will become the hostname field in the new table */
  $form['computing']['hostname'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#default_value' => $edit['hostname'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The asset's hostname"),  
  );

  $form['computing']['os'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Operating System'),
    '#default_value' => $edit['os'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The installed operating system e.g. 'Solaris 9'"),
  );

  $form['computing']['processor'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Processor'),
    '#default_value' => $edit['processor'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("Asset's Processor e.g. 'UltraSparc II'"),
  );

  $form['computing']['clockspeed'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Clock Speed'),
    '#default_value' => $edit['clockspeed'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The processor clock speed e.g. '2x 1.34GHz'"),
  );

  $form['computing']['ramtype'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('RAM Type'),
    '#default_value' => $edit['ramtype'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The type of RAM required e.g 'SODIMM DDR'"),
  );

  /* might it be an idea to have the currently fitted ammount followed by the maximum ammount */
  $form['computing']['ramamount'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('RAM amount'),
    '#default_value' => $edit['ramamount'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The amount of RAM currently fitted e.g. '512M + 256M'"),
  );


  $form['computing']['macaddr'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('MAC Address'),
    '#default_value' => $edit['macaddr'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The Media Access Control Address e.g. '00-08-74-4C-7F-1D'"),
  );

  $form['computing']['ipaddr'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Primary IP Address'),
    '#default_value' => $edit['ipaddr'],
    '#size' => 16,
    '#maxlength' => 16,
    '#description' => t("The asset's primary IP Address e.g '192.168.26.208'"),
  );
  $form['computing']['addipaddr'] =  array(
//   '#prefix' => '<div class="erm_normal_left">',
//    '#suffix' => '</div>',
    '#type' => 'textarea',
    '#title' => t('Additional IP Addresses'),
    '#default_value' => $edit['addipaddr'],
    '#resizable' => FALSE,
    '#cols' => 18,
    '#rows' => 3,
    '#description' => t("Any additional IP Addresses"),
  );

  $form['computing']['rack'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Rack'),
    '#default_value' => $edit['rack'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The rack where the equipment is located"),
  );

  $form['computing']['dnsflag'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'checkbox',
    '#title' => t('Provide DNS'),
    '#default_value' => $edit['dnsflag'],
    '#description' => t("Is DNS required? (checked = yes)"),
  );

  /* ZZZZZZZZZZZZZ it sounds as though this field isn't used anymore (since everything's now ethernet) could make the label configurable */
/* NO LONGER USED 
  $form['computing']['asset_nictype'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Network Adaptor type'),
    '#default_value' => $edit['asset_nictype'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The network adaptor type"),
  );

*/

/* ZZZZZZZZZZZ
  $form['computing']['lastinspected'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Last Inspected Date'),
    '#default_value' => $edit['lastinspected'],
    '#size' => 8,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%Y-%m-%d',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("Date the equipment was last electrically inspected as yyyy-mm-dd"),
  );
*/
  return $form;
}


/* accounting fields for the asset form */
function ownership_form() {
  $form = array();

  $form['ownership'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => 'Ownership Details',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );


  /* ZZZZ we'll need a select statement to populate a drop-down */
  /* we'll have to present the user with a pulldown list of all the currently valid projects - these are actually stored in 
  the erm_profit_jobs table 
  I believe the status column in the jobs table shows which jobs/projects are active (1) */
  /* NOTE that by selecting the list in this way, if the currect value isn't one that's valid then it can't be shown in the list */

  $sql = "SELECT jid, jname, status, jobtype FROM {erm_profit_jobs} WHERE status = 1 AND jobtype > 0 ORDER BY jid";
  $queryResult = db_query($sql);

  /* lets test how many records are returned */
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    drupal_set_message("Error:The profit_jobs table does not contain any entries");
    $projectlist['0'] = t("No jobs exist");
  }
  else {
    $projectlist['0'] = t("Not Applicable");

    while ($links = db_fetch_object($queryResult)) {
      $projectlist[$links->jid] = $links->jid . " | " . $links->jname;
    }
  }

  $form['ownership']['prjid'] =  array(
  '#prefix' => '<div class="erm_normal_left">',
  '#suffix' => '</div>',
  '#type' => 'select',
  '#title' => t('Project'),
  '#options' => $projectlist,
  '#default_value' => $edit['prjid'],
  '#description' => t("The project that controls the asset"),
  );

  /* NO idea what these start and end's are for - I suppose I can understand start but won't that be the same
  as the invoice date or the purchase date? End?? is that the time when the asset has depreciated? is it a tax thing? should 
  it be in accounting */

/* as I suspected - the start and end dates DON't belong here - they'll live in another table to control the booking of assets to projects */

  /* ZZZZ we need the JS date select here */
/* $form['ownership']['startdate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Start Date'),
    '#default_value' => $edit['startdate'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The start date of the asset"),
  );


  /* ZZZZ we need the JS date select here */
/* $form['ownership']['enddate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('End Date'),
    '#default_value' => $edit['enddate'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The end date of the asset"),
  );
*/

  /* the person responsible for the asset - what we need here is a select box allowing any user to be selected
  that could be a very long select! */
  $sql = "SELECT uid, name FROM {users} ORDER BY name";
  $queryResult = db_query($sql);


  while ($links = db_fetch_object($queryResult)) {
    $userlist[$links->uid] = $links->name;
  }

  $form['ownership']['user_id'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#title' => t('Owner'),
    '#options' => $userlist,
    '#default_value' => $edit['user_id'],
    '#description' => t("The person responsible for the asset"),
  );
  return $form;
}



/* accounting fields for the asset form */
function accounting_form() {
  $form = array();

  $form['accounting_details'] = array(
    '#type' => 'fieldset',
    '#title' => 'Accounting Details',
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['accounting_details']['acctinvnum'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Invoice Number'),
    '#default_value' => $edit['acctinvnum'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The invoice number associated with the asset"),
  );
  $form['accounting_details']['ponumber'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Purchase Order Number'),
    '#default_value' => $edit['ponumber'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The purchase order number associated with the asset"),
  );
/* ZZZZZZZZZZ
  $form['accounting_details']['acctinvdate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Invoice Date'),
    '#default_value' => $edit['acctinvdate'],
    '#size' => 8,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%Y-%m-%d',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The Original invoice date as yyyy-mm-dd"),
  );

  $form['accounting_details']['purchasedate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Purchase Date'),
    '#default_value' => $edit['purchasedate'],
    '#size' => 8,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%Y-%m-%d',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The Purchase date as yyyy-mm-dd"),
  );
*/

  $form['accounting_details']['acctinvval'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Invoice Value'),
    '#default_value' => $edit['acctinvval'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The Invoice Value"),         // ZZZZ this should be a decimal 10,2
  );

  /* ZZZZ this is a pull-down array of possible currency types - a static list - it will be stored in the database
  as an integer. For edit, code to go here for initial value */
  $currencylist = array(
    '1' => t('Yen - Japan'),
    '2' => t('£Sterling - UK'),
    '3' => t('Dollars - US'),
    '4' => t('Euro - Europe'),
  );

  $form['accounting_details']['acctcurflag'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $currencylist,    // ZZZZ !!
    '#title' => t('Currency Type'),
    '#default_value' => $edit['acctcurflag'],
    '#description' => t("The Currency Type - e.g. UK Sterling"),
  );

  /* ZZZZ shall we make this a checkbox with the default value being unchecked */
  $form['accounting_details']['acctprjuse'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'checkbox',
    '#title' => t('Cost to customer'),
    '#default_value' => $edit['acctprjuse'],
    '#description' => t("Cost to customer (checked = yes)"),
  );

  /* the supplier field in the original database is now replaced by a supplier id with a new
  table, erm_supplier, holding the list of valid suppliers */
  /* ZZZZ need to fill the supplier list */
  $supplierlist = array(
    '1' => t('Hewlett Packard'),
    '2' => t('Microsoft'),
    '3' => t('Dudley Stationers'),
    '4' => t('Viking Direct'),
    '5' => t('BT'),
    '6' => t('Fujitsu'),
    '7' => t('Sun Microsystems'),
  );


  $form['accounting_details']['supplierid'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $supplierlist,    // ZZZZ !!
    '#title' => t('Supplier'),
    '#default_value' => $edit['supplierid'],
    '#description' => t("The Supplier"),
  );
  /* I suppose we could have a 'show supplier details' button too? */

  return $form;

}
