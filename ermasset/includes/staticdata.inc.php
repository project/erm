<?php

// implement a menu - not in here though, the menu will have to be in the module itself */



// ZZZZ another optional value needed - to display or not the hidden records considering making another function to deal with 
// hidden records - perhaps we should do this by having extra functions to deal with hidden options

// the submit function is used to submit changes to the database, it takes the following parameters:
// the modulename, the tablename, the mode (whether edit or add), the perms (which fields to change) and the data to insert
function sFormSubmit($modulename, $tablename, $mode, $perms, $formvals) {

  $fields = "";    // a string which will contain the entire fieldlist, comma seperated, within brackets
  $fieldcount = 0;  // a count of the number of fields we are commiting to the database
  $initial = "";    // the initial part of the query string
  $values1 = "";    // these are actual field values 
  $evalit = "";     // a string which needs evaluation to return the actual form value
  $oneval = "";     // an individual value as a result of the eval 
  $wherebit = "";   // the where clause for an update

  // get back a list of the fields we're inserting
  $fieldlist = sGetTable($modulename, $tablename, $perms);

  if ($mode == 'edit') {
    $initial .= t("UPDATE {" . $tablename . "} SET ");
    // the update statement is in the format <fieldname> = '<fieldvalue>', WHERE id = <value>

    // get each required field from the table
    while ($sdata = db_fetch_object($fieldlist)) {

      $evalit =  t('$oneval = $formvals["' . $sdata->snamekey . '"];');
      eval($evalit);
      if ($fieldcount++ == 0) {
        $wherebit = t(" WHERE " . $sdata->snamekey . " = '" . $oneval . "'");
      }
      else {
        if ($oneval != NULL) {
          if ($fieldcount++ > 2) {
            $fields .= t(", " . $sdata->snamekey . " = '" . $oneval . "'" );
          }
          else {
            $fields .= t($sdata->snamekey . " = '" . $oneval . "'" );
          }
        }
      }
    }
    $querystring = t($initial . $fields . $wherebit);
  }
  else {
    $initial .= t('INSERT INTO {' . $tablename . '} (');
    $values1 .= t(') VALUES (');
    
    // get each required field from the table
    while ($sdata = db_fetch_object($fieldlist)) {
      $evalit =  t('$oneval = $formvals["' . $sdata->snamekey . '"];');
      eval($evalit);
      if ($oneval != NULL) {
        if ($fieldcount++) {
          $fields .= t(", " . $sdata->snamekey);
          $values1 .= t(", '" . $oneval . "'");
        }
        else {
          $fields .= t($sdata->snamekey);
          $values1 .= t("'" . $oneval . "'");
        }
      }
    }
    $querystring = t($initial . $fields . $values1 . ');');
  }
  db_query($querystring);
  return 1;    // ZZ I'll try to return a value to indicate whether the query was successful
}


//nb perms is now non-optional
// this function returns a form array. The parameters passed are parent module, table name, perm (if perm is NULL or contains 0
// then all records are included) and an array of asset values (this is only filled in when we're editing a record - not for new 
// records)
function sBuildForm($modulename, $tablename, $functionset, $perms, $rowdata = NULL) {

//ZZZ might be worth preloading a functionlist - would mean a lot less database accesses and probably a faster form load

  // retrieve information on this table from the static_data table 
  $xx = array();
  $fieldlist = sGetTable($modulename, $tablename, $perms);

  // for each field entry we need to use the appropriate function to generate a form element
  while ($sdata = db_fetch_object($fieldlist)) {
    $fieldfunction = sGetOneFunction($modulename, $sdata->stype, $functionset);
    // call the function with values $sdata and $val - note that for fieldsets there will be no matching value
    if ($sdata->stype == 7) {
      $keystring = t('$xx = ' . $fieldfunction . '($sdata);');
      eval($keystring);
    }
    else {
      if ($rowdata != NULL) {
        //we have to find the matching value in the $links array for use as the default value
        $keystring = t('$val = $rowdata->' . $sdata->snamekey . ';');
        eval($keystring);
        $keystring = t('$xx["$sdata->snamekey"] = ' . $fieldfunction . '($sdata,"' . $val . '");');
        eval($keystring);
      }
      else {
      // there is no default value
        $keystring = t('$xx["$sdata->snamekey"] = ' . $fieldfunction . '($sdata,NULL);');
        eval($keystring);
      }
    }
  }
  // return the form array
  return $xx;
}

// given the name of the parent module and the (optional) functionset name this will return an array of functions
function sGetFunctions($modulename, $functionset = NULL) {
  // start by getting the global default function list
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '10' AND snamekey = 'default'");
  $queryResult = db_query($myQuery);
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    drupal_set_message("ERROR:No default function list found in static data table");
    // nothing we can do - what should we return?
    return NULL;
  }
  $oneRecord = db_fetch_object ($queryResult);
  // get the default functions
  $myQuery = t("SELECT snamekey, svalue FROM  {static_data} WHERE slevel = '11' AND sparentid = '" . $oneRecord->sdid . "'" );
  $queryResult = db_query($myQuery);

  // load the functionlist into an array
  $functions = array();
  while ($oneRecord = db_fetch_object ($queryResult)) {
    $functions[$oneRecord->snamekey] = $oneRecord->svalue;
  }
  if ($functionset == NULL) {
    return $functions;
  }
  if ($functionset == 'default') {
    return $functions;
  }
  
  // now get the function list that was asked for - find the ID of the parent module 
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel < '2' AND snamekey = '" . $modulename ."'");
  $queryResult = db_query($myQuery);
  // we could put loads of error checking here but I don't think it's necessary - whoever's coding or setting up the app 
  //should check the entries are present in the table 
  $oneRecord = db_fetch_object ($queryResult);

  // stage 2 - having found the parent, lets find the id  of the functionset itself 
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '10' AND snamekey = '" . $functionset . "' AND sparentid = '" . $oneRecord->sdid . "'" );
  $queryResult = db_query($myQuery);
  // if we couldn't find the functionset specified so we'll just carry on with the default set
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    return $functions;
  }
  /* again, we should have found one entry - the parent record of the function set */
  $oneRecord = db_fetch_object ($queryResult);

  /* retrieve the function list */
  $myQuery = t("SELECT snamekey, svalue FROM  {static_data} WHERE shidden = '0' AND slevel = '11' AND sparentid = '" . $oneRecord->sdid . "'");
  $queryResult = db_query($myQuery);
  // if we have functions in the range 0-9 they will overwrite the default functions
  while ($oneRecord = db_fetch_object ($queryResult)) {
    $functions[$oneRecord->snamekey] = $oneRecord->svalue;
  }

  return $functions;
}

// this is similar to the function above but it takes an extra parameter, the control type, and returns a function string 
// instead of an array
function sGetOneFunction($modulename, $type, $functionset = 'default') {
  $notdoneit = TRUE;

  while ($notdoneit) {
    // establish whether they just want to use the global functions
    if ($functionset == 'default') {
      $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '10' AND snamekey = 'default'");
      $queryResult = db_query($myQuery);
      $numret = db_num_rows($queryResult);
      if ($numret == 0) {
        drupal_set_message("ERROR:No default function list found in static data table");
        // nothing we can do - what should we return?
        return NULL;
      }
      $oneRecord = db_fetch_object ($queryResult);
      // get the default function requested
      $myQuery = t("SELECT svalue FROM  {static_data} WHERE slevel = '11' AND sparentid = '" . $oneRecord->sdid . "' AND snamekey = '" . $type . "'");

      $queryResult = db_query($myQuery);
      $numret = db_num_rows($queryResult);
      if ($numret == 0) {
        drupal_set_message("ERROR: No function found in defaults for type: " . $type);
        $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '11' AND sparentid = '" . $oneRecord->sdid . "' AND snamekey = '0'");
        $queryResult = db_query($myQuery);
        $numret = db_num_rows($queryResult);
        if ($numret == 0) {
          drupal_set_message("ERROR:No entries found in default functionlist");
          // nothing we can do - what should we return?
          return NULL;
        }
      }
      $functionrec = db_fetch_object ($queryResult);
      $notdoneit = FALSE;
    }
    else {
     // they don't want the default so we have to see whether the functionset they've asked for is available - if it's
      // not then load the  default set, if the default set's not available then thats a BIG error 

      // now get the function list that was asked for - find the ID of the parent module 
      $myQuery = t("SELECT * FROM  {static_data} WHERE slevel < '2' AND snamekey = '" . $modulename ."'");
      $queryResult = db_query($myQuery);
      // we could put loads of error checking here but I don't think it's necessary - whoever's coding or setting up the app 
      //should check the module entries are present in the table 
      $oneRecord = db_fetch_object ($queryResult);

      // stage 2 - having found the parent,module lets find the id  of the functionset itself 
      $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '10' AND snamekey = '" . $functionset . "' AND sparentid = '" . $oneRecord->sdid . "'" );
      $queryResult = db_query($myQuery);
      // if we couldn't find the functionset specified so we'll just carry on with the default set
      $numret = db_num_rows($queryResult);
      if ($numret == 0) {
        drupal_set_message("ERROR:Could not find functionset = " . $functionset . ", default functionlist will be used");
        // we'll go round the while loop again this time looking for the default set
        $functionset = 'default';
      }
      else {
       // again, we should have found one entry - the parent record of the function set 
        $oneRecord = db_fetch_object ($queryResult);

        /* retrieve the individual function name */
        $myQuery = t("SELECT svalue FROM  {static_data} WHERE slevel = '11' AND sparentid = '" . $oneRecord->sdid . "' AND snamekey = '" . $type . "'");
        $queryResult = db_query($myQuery);
        $numret = db_num_rows($queryResult);
        if ($numret == 0) {
          //it's not in the functionset it must be in the default set
          $functionset = 'default';
        }
        else {
          $notdoneit = FALSE;
          $functionrec = db_fetch_object ($queryResult);
        }
      }
    }
  }
  $functioname = $functionrec->svalue;

  return $functioname;

}


 /* function to search through the static data table looking for the table specified which has the parentmodule specified - 
 and only output the records specified in the array perm. This returns an array of records 
Note that, because a table can be parented from a package or module, the module will inherit all the package tables */
function sGetTable($modulename, $tablename, $perms = NULL) {
/* ZZZZZZ this MUST be rewritten to build a function array rather than picking off one function at a time which is dB intensive */
  /* perm is an array of permission numbers, we create a query string from the values in it. If perm is empty or
  zero we don't need to do anything */
  $flag = 0;
  if ($perms != NULL) {
    foreach ($perms as $value) {
      if ($flag) $wherestring .= " OR sperm = '" . $value . "'";
      else $wherestring .= " AND sperm = '" . $value . "'";
      $flag++;
    }
  }
  /* find the ID of the parent module */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel < '2' AND snamekey = '" . $modulename ."'");
  $queryResult = db_query($myQuery);
  /* we could put loads of error checking here but I don't think it's necessary - whoever's coding or setting up the app should check 
  the entries are present in the table */
  $oneRecord = db_fetch_object ($queryResult);

  // just keep a note of its parentid
  $parentid = $oneRecord->sparentid;

  /* stage 2 - having found the parent, lets find the id  of the table itself */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '6' AND snamekey = '" . $tablename . "' AND sparentid = '" . $oneRecord->sdid . "'" );
 
  $queryResult = db_query($myQuery);
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    // theres no table at module level, lets see if we've inherited it
    $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '6' AND snamekey = '" . $tablename . "' AND sparentid = '" . $parentid . "'" );

    $queryResult = db_query($myQuery);
    $numret = db_num_rows($queryResult);
    if ($numret == 0) {
      // theres no table at package level, lets see if it's global
      $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '6' AND snamekey = '" . $tablename . "' AND sparentid = '0'" );
      $queryResult = db_query($myQuery);
      $numret = db_num_rows($queryResult);
      if ($numret == 0) {
        // can't find the table at all - give an arror message and return NULL
        drupal_set_message("FATAL ERROR:Could not find table = " . $tablename);
        return NULL;
      }
    }
  }

  // again, we should only be finding one entry 
  $oneRecord = db_fetch_object ($queryResult);
  $listlevel = $oneRecord->slevel + 1;

  // this time we go and find the list itself,  $flag indicate that we're only bringing back certain values of perm
  if ($flag) {
    $myQuery = t("SELECT * FROM  {static_data} WHERE shidden = '0' AND slevel = '7' AND sparentid = '" . $oneRecord->sdid . "'" . $wherestring . " ORDER BY sweight");
  }
  else {
    $myQuery = t("SELECT * FROM  {static_data} WHERE shidden = '0' AND slevel = '7' AND sparentid = '" . $oneRecord->sdid . "' ORDER BY sweight");
  }

  $queryResult = db_query($myQuery);

  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    drupal_set_message("ERROR: No table entries found for " . $tablename);
    return NULL;
  }
  return $queryResult;
}

 /* function to search through the static data table looking for the individual field record from the table specified which has the
 parentmodule specified - This returns a record array  */
function sGetTableEntry($modulename, $tablename, $record) {

  /* find the ID of the parent module */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel < '2' AND snamekey = '" . $modulename ."'");
  $queryResult = db_query($myQuery);
  $numret = db_num_rows($queryResult);
  if ($numret == 0) drupal_set_message("No static data modulename found");

  // we could put loads of error checking here but I don't think it's necessary - whoever's coding or setting up the app should check 
  // the entries are present in the table 
  $oneRecord = db_fetch_object ($queryResult);

  // stage 2 - having found the parent, lets find the id  of the listname itself 
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '6' AND snamekey = '" . $tablename . "' AND sparentid = '" . $oneRecord->sdid . "'" );
 
 $queryResult = db_query($myQuery);
  $numret = db_num_rows($queryResult);
  if ($numret == 0) drupal_set_message("No static data listname found");

  // again, we should only be finding one entry 
  $oneRecord = db_fetch_object ($queryResult);
  $listlevel = $oneRecord->slevel + 1;

  // this time we go and find the record itself 
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '" . $listlevel . "' AND sparentid = '" . $oneRecord->sdid . "' AND snamekey = '" . $record . "'");

  // zzzz make sure we've found something if not we should return NULL or similar
  $queryResult = db_query($myQuery);
  $numret = db_num_rows($queryResult);
  if ($numret == 0) drupal_set_message("No static data record found");

  return $queryResult;

}


/* function to search through the static data table looking for the list specified which has the parentmodule specified - 
and getting the text value of the key specified. This returns a string */
function sGetListValue($modulename, $listname, $val_in) {

  /* find the ID of the parent module */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel < '2' AND snamekey = '" . $modulename ."'");
  $queryResult = db_query($myQuery);
  /* we could put loads of error checking here but I don't think it's necessary - whoever's coding or setting up the app should check 
  the entries are present in the table */
  $oneRecord = db_fetch_object ($queryResult);

  /* stage 2 - having found the parent, lets find the id  of the listname itself */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '4' AND snamekey = '" . $listname . "' AND sparentid = '" . $oneRecord->sdid . "'" );
 
 $queryResult = db_query($myQuery);

  /* if the select returns no results then we'll search all the possible lists for a matching name */
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '4' AND snamekey = '" . $listname . "'"); 
    $queryResult = db_query($myQuery);
    $numret = db_num_rows($queryResult);
    if ($numret == 0) {
      drupal_set_message("ERROR:list - " . $listname . " - not found in static data table");
      return("");
    }
  }

  /* again, we should only be finding one entry */
  $oneRecord = db_fetch_object ($queryResult);
  $listlevel = $oneRecord->slevel + 1;

  /* this time we go and find the individual entry in the list */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '" . $listlevel . "' AND sparentid = '" . $oneRecord->sdid . "' AND snamekey = '" . $val_in . "'");

  $queryResult = db_query($myQuery);

  /* again, we should only be finding one entry */
  $oneRecord = db_fetch_object ($queryResult);

  $val_out = $oneRecord->svalue;

  // return the individual record from the list 
  return $val_out;

}

/* function to search through the static data table looking for the list specified which has the parentmodule specified - 
 This returns a key / value array */
function sGetList($modulename, $listname) {

  /* find the ID of the parent module */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel < '2' AND snamekey = '" . $modulename ."'");
  $queryResult = db_query($myQuery);
  /* we could put loads of error checking here but I don't think it's necessary - whoever's coding or setting up the app should check 
  the entries are present in the table */
  $oneRecord = db_fetch_object ($queryResult);

  /* stage 2 - having found the parent, lets find the id  of the listname itself */
  $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '4' AND snamekey = '" . $listname . "' AND sparentid = '" . $oneRecord->sdid . "'" );
 
  $queryResult = db_query($myQuery);

  /* if the select returns no results then we'll search all the possible lists for a matching name */
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    $myQuery = t("SELECT * FROM  {static_data} WHERE slevel = '4' AND snamekey = '" . $listname . "'"); 
    $queryResult = db_query($myQuery);
    $numret = db_num_rows($queryResult);
    if ($numret == 0) {
      drupal_set_message("ERROR:list not found in static data table - " . $listname);
      return("");
    }
  }

  /* again, we should only be finding one entry */
  $oneRecord = db_fetch_object ($queryResult);
  $listlevel = $oneRecord->slevel + 1;

  /* this time we go and bring back the list entries*/
  $myQuery = t("SELECT snamekey, svalue FROM  {static_data} WHERE slevel = '5' AND sparentid = '" . $oneRecord->sdid . "' AND shidden = '0' ORDER BY sweight");

  $queryResult = db_query($myQuery);

  $listout = array();
  /* this time we'll have a whole load of entries */
  while ($oneRecord = db_fetch_object ($queryResult)) {
    $listout[$oneRecord->snamekey] = $oneRecord->svalue;
  }

  // return the list of records  
  return $listout;

}

/* FUNCTIONS TO BUILD FORM ELEMENTS - each function is passed a record from the static data table, and the current value of 
the element - each returns an element array, some functions (the complex select) have an additional parameter */
function sTextfield($sdata,$val = NULL) {
  $xx = array();

  $xx[$sdata->snamekey] =  array(
    '#type' => 'textfield',
    '#title' => $sdata->svalue,
    '#size' => $sdata->ssize,
    '#maxlength' => $sdata->smaxlen,
    '#description' => $sdata->sdesc,
  );
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx[$sdata->snamekey]['#prefix'] = $myformat;
    $xx[$sdata->snamekey]['#suffix'] = '</div>';
  } 

  if ($val == NULL) {
    // for a new record there maybe a default value held in the static data record
    if ($sdata->snamekey != NULL) {
      $xx[$sdata->snamekey]['#default_value'] = $sdata->sdefault; 
    }
  }
  else {
    $xx[$sdata->snamekey]['#default_value'] = $val;
  }

  return $xx;
}

/* for a text area the size and maxlen control the rows and columns */
function sTextarea($sdata,$val = NULL) {
  $xx = array();

  $xx[$sdata->snamekey] =  array(
    '#type' => 'textarea',
    '#title' => $sdata->svalue,
    '#rows' => $sdata->ssize,
    '#cols' => $sdata->smaxlen,
    '#resizable' => FALSE,
    '#description' => $sdata->sdesc,
  );
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx[$sdata->snamekey]['#prefix'] = $myformat;
    $xx[$sdata->snamekey]['#suffix'] = '</div>';
  } 
  if ($val == NULL) {
    // for a new record there maybe a default value held in the static data record
    if ($sdata->snamekey != NULL) {
      $xx[$sdata->snamekey]['#default_value'] = $sdata->sdefault; 
    }
  }
  else {
    $xx[$sdata->snamekey]['#default_value'] = $val;
  }

  return $xx;
}

function sSimpleSelect($sdata,$val = NULL) {
  $xx = array();

  // this time we need to populate the options array
  $listoptions = sGetList('ermasset', $sdata->slistname);

  $xx[$sdata->snamekey] =  array(
    '#type' => 'select',
    '#title' => $sdata->svalue,
    '#options' => $listoptions,
    '#description' => $sdata->sdesc,
  );
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx[$sdata->snamekey]['#prefix'] = $myformat;
    $xx[$sdata->snamekey]['#suffix'] = '</div>';
  } 
  if ($val == NULL) {
    // for a new record there maybe a default value held in the static data record
    if ($sdata->snamekey != NULL) {
      $xx[$sdata->snamekey]['#default_value'] = $sdata->sdefault; 
    }
  }
  else {
    $xx[$sdata->snamekey]['#default_value'] = $val;
  }

  return $xx;
}

function sCheckBox($sdata,$val = NULL) {
  $xx = array();

  $xx[$sdata->snamekey] =  array(
    '#type' => 'checkbox',
    '#title' => $sdata->svalue,
    '#description' => $sdata->sdesc,
  );
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx[$sdata->snamekey]['#prefix'] = $myformat;
    $xx[$sdata->snamekey]['#suffix'] = '</div>';
  } 
  if ($val == NULL) {
    // for a new record there maybe a default value held in the static data record
    if ($sdata->snamekey != NULL) {
      $xx[$sdata->snamekey]['#default_value'] = $sdata->sdefault; 
    }
  }
  else {
    $xx[$sdata->snamekey]['#default_value'] = $val;
  }

  return $xx;
}

function sFieldset($sdata) {
  $xx = array();

  $xx =  array(
    '#type' => 'fieldset',
    '#title' => $sdata->svalue,
    '#collapsible' => $sdata->ssize,
    '#collapsed' => $sdata->smaxlen,
  );
  // I'm not sure .. does a fieldset have formatting? I'll have to check
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx['#prefix'] = $myformat;
    $xx['#suffix'] = '</div>';
  } 
  return $xx;
}

function sJSDate($sdata,$val = NULL) {
  $xx = array();

  $xx[$sdata->snamekey] =  array(
    '#type' => 'textfield',
    '#title' => $sdata->svalue,
    '#size' =>  $sdata->ssize,
    '#maxlength' => $sdata->smaxlen,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%Y-%m-%d',
    '#jscalendar_showsTime' => 'false',
    '#description' => $sdata->sdesc,
  );
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx[$sdata->snamekey]['#prefix'] = $myformat;
    $xx[$sdata->snamekey]['#suffix'] = '</div>';
  } 
  if ($val == NULL) {
    // for a new record there maybe a default value held in the static data record
    if ($sdata->snamekey != NULL) {
      $xx[$sdata->snamekey]['#default_value'] = $sdata->sdefault; 
    }
  }
  else {
    $xx[$sdata->snamekey]['#default_value'] = $val;
  }

  return $xx;
}

// we could use this to do the grunt work with individual functions doing the query and choosing  the fields to go into the 
// options
function sComplexSelect($sdata,$optionlist,$val) {
  // specialised select statement - get the parent asset
  $xx = array();

  $xx[$sdata->snamekey] =  array(
    '#type' => 'select',
    '#title' => $sdata->svalue,
    '#options' => $optionlist,
    '#description' => $sdata->sdesc,
  );
  if ($sdata->sformat != NULL) {
    $myformat = t("<div class='" . $sdata->sformat . "'>");
    $xx[$sdata->snamekey]['#prefix'] = $myformat;
    $xx[$sdata->snamekey]['#suffix'] = '</div>';
  } 
  if ($val == NULL) {
    // for a new record there maybe a default value held in the static data record
    if ($sdata->snamekey != NULL) {
      $xx[$sdata->snamekey]['#default_value'] = $sdata->sdefault; 
    }
  }
  else {
    $xx[$sdata->snamekey]['#default_value'] = $val;
  }

  return $xx;
}


function sUserSelect($sdata,$val = NULL) {
  // specialised select statement - get the users
  $sql = "SELECT uid, name FROM {users} ORDER BY name";
  $queryResult = db_query($sql);
  while ($links = db_fetch_object($queryResult)) {
    $userlist[$links->uid] = $links->name;
  }
  $xx = sComplexSelect($sdata, $userlist, $val);

  return $xx;
}

function sAssetSelect($sdata,$val = NULL) {
  $sql = "SELECT id, name FROM {erm_asset} ORDER BY id";
  $queryResult = db_query($sql);
  $assetlist = array();
  $assetlist[0] = t("N/A");
  while ($links = db_fetch_object($queryResult)) {
    $assetlist[$links->id] = $links->id . " | " . $links->name;
  }

  $xx = sComplexSelect($sdata, $assetlist, $val);

  return $xx;
}

function sProjectSelect($sdata,$val = NULL) {
  $sql = "SELECT jid, jname, status, jobtype FROM {erm_profit_jobs} WHERE status = 1 AND jobtype > 0 ORDER BY jid";
  $queryResult = db_query($sql);
  $projectlist = array();
  $projectlist[0] = t("N/A");
  while ($links = db_fetch_object($queryResult)) {
    $projectlist[$links->jid] = $links->jid . " | " . $links->jname;
  }

  $xx = sComplexSelect($sdata, $projectlist, $val);

  return $xx;
}

function sSupplierSelect($sdata,$val = NULL) {
  $sql = "SELECT supid, name FROM {erm_supplier} ORDER BY name";
  $queryResult = db_query($sql);
  $numret = db_num_rows($queryResult);
  if ($numret == 0) {
    // should NEVER happen
    drupal_set_message("No suppliers found");
    return NULL;
  }
  $supplierlist = array();
  $supplierlist[0] = t("N/A");
  while ($links = db_fetch_object($queryResult)) {
    $supplierlist[$links->supid] = $links->name;
  }
  $xx = sComplexSelect($sdata, $supplierlist, $val);

  return $xx;
}
// ZZZZ this one's not finished, it will need additional parameters because it relies on values from the previous select...
function sLocationSelect($sdata,$val = NULL) {
  $query = "SELECT loc_id, description FROM {erm_location}";
  $queryResult = db_query($sql);
  $locationlist = array();
  $locationlist[0] = t("N/A");
  while ($links = db_fetch_object($queryResult)) {
    $locationlist[$links->locid] = $links->description;
  }
  $xx = sComplexSelect($sdata, $locationlist, $val);

  return $xx;
}
