<?php

/* the parameters for the location form are a flag to say whether the form is collapsed or not, and the set of database values
to populate the form */
function location_form_page($collapsed = FALSE) {


  /* we should actually be testing for the presence of the activeselect module here */
  $activeselect = TRUE;

 /* lets try getting all the options for the selects from the database - yes I know I should be putting %d's and %s's in
  the select statements, I just did things quick and dirty */
   $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 0 AND loc_id = 0"); 
  $queryResult = db_query($myQuery);

 while ( $oneRecord = db_fetch_object ($queryResult) ) {
    $countrylist[$oneRecord->loc_id] = $oneRecord->description; 
 }

  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 0 AND loc_id > 0 ORDER BY description"); 
  $queryResult = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($queryResult) ) {
    $countrylist[$oneRecord->loc_id] = $oneRecord->description; 
  }

  /* my understanding is that you don't actually NEED to prepopulate these selects with the available options ... but
  if you don't then drupal's validation will fail. But of course, what you can do is switch off the validation
  by adding '#DANGEROUS_SKIP_CHECK' => TRUE, of course then you'll need to add your own validation */
  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 1 ORDER BY description"); 
  $townquery = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($townquery) ) {
    $townlist[$oneRecord->loc_id] = $oneRecord->description; 
  }
  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 2 ORDER BY description"); 
  $buildingquery = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($buildingquery) ) {
    $buildinglist[$oneRecord->loc_id] = $oneRecord->description; 
  }

  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 3 ORDER BY description"); 
  $roomquery = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($roomquery) ) {
    $roomlist[$oneRecord->loc_id] = $oneRecord->description; 
  }

  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => 'Select location',
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed
  );

  
  $form['location']['countryid'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => $countrylist,
//  '#default_value' => '00004', if you're putting in a default value it has to be the key value. i.e not 'United Kingdom' but '00004'
    '#required' => FALSE,
    '#DANGEROUS_SKIP_CHECK' => TRUE,
 );
  if ($activeselect) {
    $form['location']['countryid']['#type'] = 'activeselect';
    $form['location']['countryid']['#activeselect_path'] = 'erm/asset/new/activeselect';
    $form['location']['countryid']['#activeselect_targets'] = 'townid';
    $form['location']['countryid']['#activeselect_extra'] = '';
  }

   $form['location']['townid'] = array(
    '#type' => 'select',
    '#title' => t('Town'),
    '#options' => $townlist,
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#DANGEROUS_SKIP_CHECK' => TRUE,
  );
 if ($activeselect) {
    $form['location']['townid']['#type'] = 'activeselect';
    $form['location']['townid']['#activeselect_path'] = 'erm/asset/new/activeselect';
    $form['location']['townid']['#activeselect_targets'] = 'buildingid';
    $form['location']['townid']['#activeselect_extra'] = '';
  }

  $form['location']['buildingid'] = array(
    '#type' => 'select',
    '#title' => t('Building'),
    '#options' => $buildinglist,
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#DANGEROUS_SKIP_CHECK' => TRUE,
  );
  if ($activeselect) {
    $form['location']['buildingid']['#type'] = 'activeselect';
    $form['location']['buildingid']['#activeselect_path'] = 'erm/asset/new/activeselect';
    $form['location']['buildingid']['#activeselect_targets'] = 'roomid';
    $form['location']['buildingid']['#activeselect_extra'] = '';
  }

  $form['location']['roomid'] = array(
    '#type' => 'select',
    '#title' => t('Room'),
    '#options' => $roomlist,
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#DANGEROUS_SKIP_CHECK' => TRUE,
  );

  return $form;
}


/* this is a bit cheap at the moment - we should do some error checking either both here and in the calling function */
function location_getnextlevel($parents) {

  /* the argument $parents is actually an array passed by the calling select */
  $count = 0;
  foreach ($parents as $key => $description) {
    /* they should only be selecting one so we'll only look at the first one */
    if ($count == 0) {
      $parentkey = $key;
    }
  }

  $children = array();
  if ($parentkey == '00000') {
    $children['00000'] = "Choose one... ";
  }
  else {
// ZZZZ new code
    $children['00000'] = "Choose one... ";
    $sql = "SELECT * FROM {erm_location} WHERE parent_id = " . $parentkey . " ORDER BY description";
    $sqlresult = db_query($sql);
    while ($onerec = db_fetch_object($sqlresult)) {
      $children[$onerec->loc_id] = $onerec->description;
      }
  }
  return $children;
}  // end of function location_getnextlevel



/* I don't fully understand how the parameters are actually passed maybe the new Pro Drupal book will tell me? */
function location_activeselect($source, $targets, $string, $extra = NULL) {

  /* make sure we've been passed some arguments */ 
  if (empty($source) || empty($targets) || empty($string)) {
    exit();
  }

  $targets = explode(',', $targets);
  $output = array();

  $array = activeselect_explode_values($string);
  $child_options = location_getnextlevel($array);

  foreach ($targets as $target) {
    $options = array();

    foreach ($array as $key => $value) {
        foreach ($child_options as $key => $value) {
          $options[$key]['value'] = $value;
        }

      /* to explain - the $key field will be the selected key value from the calling select, the $target field is the name of the control
      that you're preparing options for, the $value field is the selected value of the calling select */


    } // end arrays

    $multiple = FALSE;

    $output[$target] = array('options' => $options, 'multiple' => $multiple);
  }  //end targets

  activeselect_set_header_nocache();

  print drupal_to_js($output);
  exit();
}
