<?php

/* this contains the location form and related code - this needs to be customised with the path to the activeselect 
built into whatever calls this - how do we do a constant in PHP?? */


function zlocation_form_page() {
  /* we should actually be testing for the presence of the activeselect module here and giving a warning mesasage if it's not */
  $activeselect = TRUE;

  $form = array();

  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => 'Select Location',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );


 /* lets try getting all the options for the selects from the database - yes I know I should be putting %d's and %s's in
  the select statements, I just did things quick and dirty */
   $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 0 AND loc_id = 0"); 
  $queryResult = db_query($myQuery);

 while ( $oneRecord = db_fetch_object ($queryResult) ) {
    $countrylist[$oneRecord->loc_id] = $oneRecord->description; 
 }

  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 0 AND loc_id > 0 ORDER BY description"); 
  $queryResult = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($queryResult) ) {
    $countrylist[$oneRecord->loc_id] = $oneRecord->description; 
  }

  /* my understanding is that you don't actually NEED to prepopulate these selects with the available options ... but
  if you don't then drupal's validation will fail. But of course, what you can do is switch off the validation
  by adding '#DANGEROUS_SKIP_CHECK' => TRUE, of course then you'll need to add your own validation */
  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 1 ORDER BY description"); 
  $townquery = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($townquery) ) {
    $townlist[$oneRecord->loc_id] = $oneRecord->description; 
  }
  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 2 ORDER BY description"); 
  $buildingquery = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($buildingquery) ) {
    $buildinglist[$oneRecord->loc_id] = $oneRecord->description; 
  }

  $myQuery = t("SELECT * FROM  {erm_location} WHERE level = 3 ORDER BY description"); 
  $roomquery = db_query($myQuery);
  while ( $oneRecord = db_fetch_object ($roomquery) ) {
    $roomlist[$oneRecord->loc_id] = $oneRecord->description; 
  }

  
  $form['location']['bar-1'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => $countrylist,
    '#required' => TRUE
  );
  if ($activeselect) {
    $form['location']['bar-1']['#type'] = 'activeselect';
    $form['location']['bar-1']['#activeselect_path'] = 'ermm/asset/new/activeselect';
    $form['location']['bar-1']['#activeselect_targets'] = 'bar-2';
    $form['location']['bar-1']['#activeselect_extra'] = '';
  }

  $form['location']['bar-2'] = array(
    '#type' => 'select',
    '#title' => t('Town'),
    '#options' => $townlist,
    '#multiple' => FALSE,
    '#required' => TRUE,
  );
 if ($activeselect) {
    $form['location']['bar-2']['#type'] = 'activeselect';
    $form['location']['bar-2']['#activeselect_path'] = 'ermm/asset/new/activeselect';
    $form['location']['bar-2']['#activeselect_targets'] = 'bar-3';
    $form['location']['bar-2']['#activeselect_extra'] = '';
  }

  $form['location']['bar-3'] = array(
    '#type' => 'select',
    '#title' => t('Building'),
    '#options' => $buildinglist,
    '#multiple' => FALSE,
    '#required' => TRUE,
  );
  if ($activeselect) {
    $form['location']['bar-3']['#type'] = 'activeselect';
    $form['location']['bar-3']['#activeselect_path'] = 'ermm/asset/new/activeselect';
    $form['location']['bar-3']['#activeselect_targets'] = 'bar-4';
    $form['location']['bar-3']['#activeselect_extra'] = '';
  }

  $form['location']['bar-4'] = array(
    '#type' => 'select',
    '#title' => t('Room'),
    '#options' => $roomlist,
    '#multiple' => FALSE,
    '#required' => TRUE,
  );

  return $form;
}

/* this is a bit cheap at the moment - we should do some error checking either both here and in the calling function */
function zlocation_getnextlevel($parents) {

  /* the argument $parents is actually an array passed by the calling select */
  $count = 0;
  foreach ($parents as $key => $description) {
    /* they should only be selecting one so we'll only look at the first one */
    if ($count == 0) {
      $parentkey = $key;
    }
  }

  $children = array();
  if ($parentkey == '00000') {
    $children['00000'] = "Choose one... ";
  }
  else {
    $sql = "SELECT * FROM {erm_location} WHERE parent_id = " . $parentkey . " ORDER BY description";
    $sqlresult = db_query($sql);
    while ($onerec = db_fetch_object($sqlresult)) {
      $children[$onerec->loc_id] = $onerec->description;
      }
  }
  return $children;
}  // end of function location_getnextlevel



/* I don't fully understand how the parameters are actually passed maybe the new Pro Drupal book will tell me? */
function zlocation_activeselect($source, $targets, $string, $extra = NULL) {

  /* make sure we've been passed some arguments */ 
  if (empty($source) || empty($targets) || empty($string)) {
    exit();
  }

  $targets = explode(',', $targets);
  $output = array();

  $array = location_explode_values($string);
  $child_options = zlocation_getnextlevel($array);

  foreach ($targets as $target) {
    $options = array();

    foreach ($array as $key => $value) {
        foreach ($child_options as $key => $value) {
          $options[$key]['value'] = $value;
        }

      /* to explain - the $key field will be the selected key value from the calling select, the $target field is the name of 
     the control that you're preparing options for, the $value field is the selected value of the calling select */


    } // end arrays

    $multiple = FALSE;

    $output[$target] = array('options' => $options, 'multiple' => $multiple);
  }  //end targets

  activeselect_set_header_nocache();

  print drupal_to_js($output);

  exit();
}

/*function location_form_validate($form_id, $form_values) {
  drupal_set_message('Reached the validate');  // . $form_values['step']);
  /* test which button has been pressed, if it's add location then bring up a different form */
  /*switch() {
    case:

  } 
  return;

} */

/*function location_form_submit($form_id, $form_values) {
  drupal_set_message('Reached the submit');  // . $form_values['step']);
/*  if ($form_values['step'] > 2) {
    return 'location/going';
  } 
  return;
} */

