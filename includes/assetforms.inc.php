<?php

/* note that the previous naming convention - putting addnew everywhere - has been abandoned. This is because the same
form will probably be used for view and edit */

/* the general items which relate to most assets - takes no parameters and returns a form array */
function asset_general_form() {
  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => 'General Fields',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  /* ZZZZ temporary code to build the list of options for 'types' */
  $typelist = array(
    '1' => t('Project Development'),
    '2' => t('Desktop Computing'),
    '3' => t('Core Infrastructure'),
    '4' => t('Accessories'),
    '5' => t('Furniture'),
    '6' => t('Plant & Machinery'),
    '7' => t('Software'),
    '8' => t('Storage Media'),
    '9' => t('Expansion Card'),
    '99' => t('Miscellaneous'),
  );

  /* typeflag is the ID of the asset type e.g furniture / computing / software etc. */
  /* ZZZZ we need to retrieve the static list of asset types */
  $form['general']['typeflag'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $typelist,
    '#title' => t('Asset Type'),
    '#default_value' => $edit['typeflag'],
    '#description' => t("The type of asset e.g. 'Software'"),
  );


  /* a new database field - the old name field now stores the hostname (in the it_form) - when converting the old data these 
  fields will initially be the same */
  $form['general']['asset_name'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Asset Name'),
    '#default_value' => $edit['asset_name'],
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The asset name e.g. 'michaels mac'"),  /* what an appalling example !!!! */
  );

  /* description is a new field holding  the full asset  description */
  $form['general']['description'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Asset Description'),
    '#default_value' => $edit['description'],
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t("Description of the asset"),  
  );

  $form['general']['model'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Make / Model'),
    '#default_value' => $edit['model'],
    '#size' => 15,
    '#maxlength' => 25,
    '#description' => t("The asset make and model e.g. 'Sun Ultra 60'"),
  );

  $form['general']['serial'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Serial Number'),
    '#default_value' => $edit['serial'],
    '#size' => 35,
    '#maxlength' => 50,
    '#description' => t("The asset's serial number"),
  );

  /* ZZZZ - JS widget needed here - we also need to ensure that all dates are the same length  */
  $form['general']['warrantyexp'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Warranty Expiry Date'),
    '#default_value' => $edit['warrantyexp'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("Expiry Date as dd/mm/yyyy"),
  );

  /* this asset may have a parent asset - e.g if the asset is software and is installed on a computer, the parent ID will
  be the computer on which it's installed ZZZZ we'll need a select list of all the available assets - that'll be quite a list! */
 // get data from table
  $query = "SELECT id, name FROM {erm_asset} ORDER BY id";
  $queryResult = db_query($query);

  while ($links = db_fetch_object($queryResult)) {
    $assetlist[$links->id] = $links->id . " | " . $links->name;
  }

  $form['general']['parentid'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $assetlist,   // need to build an assetlist
    '#title' => t('Parent Asset'),
    '#default_value' => $edit['parentid'],
    '#description' => t("Parent Asset - e.g. for software the parent would normally be the computer on which it is installed"),
  );

  return $form;
}

function it_form() {
  $form['computing'] = array(
    '#type' => 'fieldset',
    '#title' => 'IT Related Fields',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  /* the name field from the exiting table will become the hostname field in the new table */
  $form['computing']['hostname'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#default_value' => $edit['hostname'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The asset's hostname"),  
  );

  $form['computing']['os'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Operating System'),
    '#default_value' => $edit['os'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The operating system e.g. 'Solaris 9'"),
  );

  $form['computing']['processor'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Processor'),
    '#default_value' => $edit['processor'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The Processor e.g. 'UltraSparc II'"),
  );

  $form['computing']['clockspeed'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Clock Speed'),
    '#default_value' => $edit['clockspeed'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The processor clock speed e.g. '2x 1.34GHz'"),
  );

  $form['computing']['ramtype'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('RamType'),
    '#default_value' => $edit['ramtype'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The type of RAM required e.g 'SODIMM DDR'"),
  );

  /* might it be an idea to have the currently fitted ammount followed by the maximum ammount */
  $form['computing']['ramamount'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('RAM ammount'),
    '#default_value' => $edit['ramamount'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The ammount of RAM currently fitted e.g. '512M + 256M'"),
  );


  $form['computing']['macaddr'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('MAC Address'),
    '#default_value' => $edit['macaddr'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The Media Access Control Address e.g. '00-08-74-4C-7F-1D'"),
  );

  $form['computing']['ipaddr'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Primary IP Address'),
    '#default_value' => $edit['ipaddr'],
    '#size' => 12,
    '#maxlength' => 16,
    '#description' => t("The asset's primary IP Address e.g '192.168.26.208'"),
  );
  $form['computing']['addipaddr'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textarea',
    '#title' => t('Additional IP Addresses'),
    '#default_value' => $edit['addipaddr'],
    '#description' => t("Any additional IP Addresses"),
  );

  $form['computing']['rack'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Rack'),
    '#default_value' => $edit['rack'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The rack where the equipment is located"),
  );

  /* ZZZZZZZZZZZZZ it sounds as though this field isn't used anymore (since everything's now ethernet) could make the label configurable */
/* NO LONGER USED 
  $form['computing']['asset_nictype'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Network Adaptor type'),
    '#default_value' => $edit['asset_nictype'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The network adaptor type"),
  );

*/

  return $form;
}

/* accounting fields for the asset form */
function ownership_form() {
  $form = array();

  $form['ownership'] = array(
    '#type' => 'fieldset',
    '#title' => 'Ownership Details',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );


  /* ZZZZ we'll need a select statement to populate a drop-down */
  /* we'll have to present the user with a pulldown list of all the currently valid projects - these are actually stored in 
  the erm_profit_jobs table 
  I believe the status column in the jobs table shows which jobs/projects are active (1) */
  /* NOTE that by selecting the list in this way, if the currect value isn't one that's valid then it can't be shown in the list */
    $sql = "SELECT  FROM erm_profit_jobs WHERE status = 1 AND jobtype > 0 ORDER BY weight";
    $form['ermmasset_addnew']['prjid'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
     '#type' => 'select',
    '#title' => t('Project'),
    '#options' => $projectlist,    // ZZZZ list of valid projects
    '#default_value' => $edit['prjid'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The project that controls the asset"),
  );

  /* NO idea what these start and end's are for - I suppose I can understand start but won't that be the same
  as the invoice date or the purchase date? End?? is that the time when the asset has depreciated? is it a tax thing? should 
  it be in accounting */
  /* ZZZZ we need the JS date select here */
 $form['ownership']['startdate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Start Date'),
    '#default_value' => $edit['startdate'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The start date of the asset"),
  );

  /* ZZZZ we need the JS date select here */
 $form['ownership']['enddate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('End Date'),
    '#default_value' => $edit['enddate'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The end date of the asset"),
  );

  /* ZZZZ id of the person responsible for the asset - what we need here is a select box allowing any user to be selected
  that could be a very long select! */
  $form['ownership']['user_id'] =  array(
    '#prefix' => '<div class="erm_normal_right">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#title' => t('Owner'),
    '#options' => $userlist,    // ZZZZ
    '#default_value' => $edit['user_id'],
    '#size' => 15,
    '#maxlength' => 20,
    '#description' => t("The person responsible for the asset"),
  );

}



/* accounting fields for the asset form */
function accounting_form() {
  $form = array();

  $form['accounting_details'] = array(
    '#type' => 'fieldset',
    '#title' => 'Accounting Details',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['accounting_details']['acctinvnum'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Invoice Number'),
    '#default_value' => $edit['acctinvnum'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The invoice number associated with the asset"),
  );
  $form['accounting_details']['ponumber'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Purchase Order Number'),
    '#default_value' => $edit['ponumber'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The purchase order number associated with the asset"),
  );

  /* ZZZZZZZZZZZ put a date JS element here */
  $form['accounting_details']['acctinvdate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Invoice Date'),
    '#default_value' => $edit['acctinvdate'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The Original invoice date - DD/MM/YYYY"),
  );
  /* ZZZZZZZZZZZ put a date JS element here */
  $form['accounting_details']['purchasedate'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Purchase Date'),
    '#default_value' => $edit['purchasedate'],
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => 'jscalendar'),
    '#jscalendar_ifFormat' => '%d-%m-%Y',
    '#jscalendar_showsTime' => 'false',
    '#description' => t("The Purchase date - DD/MM/YYYY"),
  );

/* ZZZZ do we need some more fields here? we've got an invoice date - I suggest we need a second date, the date it was PAID? */
  $form['accounting_details']['acctinvval'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Invoice Value'),
    '#default_value' => $edit['acctinvval'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The Invoice Value"),         // ZZZZ this should be a decimal 10,2
  );

  /* ZZZZ this is a pull-down array of possible currency types - a static list - it will be stored in the database
  as an integer. For edit, code to go here for initial value */
  $form['accounting_details']['acctcurflag'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $currencylist,    // ZZZZ !!
    '#title' => t('Currency Type'),
    '#default_value' => $edit['acctcurflag'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The Currency Type - e.g. UK Sterling"),
  );

  /* ZZZZ shall we make this a checkbox with the default value being unchecked */
  $form['accounting_details']['acctprjuse'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'checkbox',
    '#title' => t('Cost to customer'),
    '#default_value' => $edit['acctprjuse'],
    '#description' => t("Cost to customer (checked = yes"),
  );

  /* the supplier field in the original database is now replaced by a supplier id with a new
  table, erm_supplier, holding the list of valid suppliers */
  /* ZZZZ need to fill the supplier list */
  $form['accounting_details']['supplier'] =  array(
    '#prefix' => '<div class="erm_normal_left">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $supplierlist,    // ZZZZ !!
    '#title' => t('Supplier'),
    '#default_value' => $edit['supplierid'],
    '#size' => 12,
    '#maxlength' => 25,
    '#description' => t("The Supplier"),
  );
  /* I suppose we could have a 'show supplier details' button too? */

  return $form;

}