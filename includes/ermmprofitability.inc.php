<?php

function ermmprofitability_getheaders($option) {
  switch ($option) {
    case 'job codes':
      //$header = array(t('Job code'), t('Status'), t('Name'), t('Business unit'), t('Customer'), t('Group'), t('Description'), t('File'), t('Created'), t('Closed'));
      $header = array(t('Job code'), t('Status'), t('Name'), t('Business unit'), t('Customer'), t('Group'));
    break;

    case 'business units':
      $header = array (t('Id'), t('Business unit'));
    break;

    case 'customers':
      $header = array (t('Id'), t('Name'), t('Status'), t('Location'));
    break;

    case 'groups':
      $header = array (t('Id'), t('Name'));
    break;

  }

  return $header;

} // function ermmprofitability_getheaders


function ermmprofitability_getquery($option, $sort, $rowid = NULL) {

  switch ($option) {
    case 'edit job codes':
      $query = "SELECT jid, jname, jdesc, status, buid, custid, jgid, link, created, closed " .
               "FROM {erm_profit_jobs}";
      if ($sort == 'none') $sort = 'jid';
      if ($rowid) $query .= " WHERE jid = '".$rowid."'";
    break;

    case 'job codes':
      $query = "SELECT erm_profit_jobs.jid, " .
                      "erm_profit_jobs.jname, " .
                      "erm_profit_jobs.jdesc, " .
                      "erm_profit_jobs.status, " .
                      "erm_profit_businessunits.buname, " .
                      "erm_profit_customers.custname, " .
                      "erm_profit_jobgroups.jgname, " .
                      "erm_profit_jobs.jdesc, " .
                      "erm_profit_jobs.link, " .
                      "erm_profit_jobs.created, " .
                      "erm_profit_jobs.closed " .
               "FROM {erm_profit_jobs} " .
               "LEFT JOIN {erm_profit_businessunits} " .
               "ON erm_profit_jobs.buid = erm_profit_businessunits.buid " .
               "LEFT JOIN {erm_profit_customers} " .
               "ON erm_profit_jobs.custid = erm_profit_customers.custid " .
               "LEFT JOIN {erm_profit_jobgroups} " .
               "ON erm_profit_jobs.jgid = erm_profit_jobgroups.jgid";
      if ($rowid) $query .= " WHERE erm_profit_jobs.jid = '".$rowid."'";
      if ($sort == 'none') $sort = 'erm_profit_jobs.jid';
    break;

    case 'business units':
      $query = "SELECT buid, buname " .
               "FROM {erm_profit_businessunits}";
      if ($sort == 'none') $sort = 'buid';
      if ($rowid) $query .= " WHERE buid = '".$rowid."'";
    break;

    case 'customers':
      $query = "SELECT custid, custname,status,location " .
               "FROM {erm_profit_customers}";
      if ($sort == 'none') $sort = 'custid';
      if ($rowid) $query .= " WHERE custid = '".$rowid."'";
    break;

    case 'groups':
      $query = "SELECT jgid, jgname " .
               "FROM {erm_profit_jobgroups}";
      if ($sort == 'none') $sort = 'jgid';
      if ($rowid) $query .= " WHERE jgid = '".$rowid."'";
    break;
  }
  $query .= " ORDER BY ".$sort; 

  return $query;

} // function ermmprofitability_ermm_query;


function ermmprofitability_genrows($option, $links) {
  switch ($option) {
    case 'job codes':

      $filedownload = '';
      if ($links->link) $filedownload = '<a href="'.$links->link.'">Download</a>';
      $jobstatus = ($links->status == '0') ? "Closed" : "Active";
      if (arg(2) != 'jobs')  {
        $row = array('Job Code' => '<a href="'.arg(1).'/jobs/'.$links->jid.'">'.$links->jid.'</a>', 'Status' => $jobstatus, 'Name' => $links->jname, 'Business unit' => $links->buname, 'Customer' => $links->custname, 'Group' => $links->jgname);
      } else {
        $row = array('Job Code' => '<a href="'.arg(2).'/'.$links->jid.'">'.$links->jid.'</a>', 'Status' => $jobstatus, 'Name' => $links->jname, 'Business unit' => $links->buname, 'Customer' => $links->custname, 'Group' => $links->jgname);
      }
    break;

    case 'business units':
      $row = array('Id' => '<a href="'.arg(2).'/'.$links->buid.'">'.$links->buid.'</a>', 'Business unit' => $links->buname);
    break;

    case 'customers':
      $custstatus = ($links->status == '1') ? "Prospective" : ((($links->status == '2') ? "Customer" : (($links->status == '3') ? "Partner"  : "Undefined")));
      $row = array('Id' => '<a href="'.arg(2).'/'.$links->custid.'">'.$links->custid.'</a>', 'Name' => $links->custname, 'Status' => $custstatus, 'Location' => $links->location);
    break;

    case 'groups':
      $row = array('Id' => '<a href="'.arg(2).'/'.$links->jgid.'">'.$links->jgid.'</a>', 'Name' => $links->jgname);
    break;
  }

  return $row;

} //function ermmprofitability_genrows


function ermmprofitability_gettabledata($option, $sort = 'none') {

  $output = '';
  $rows = array();

  // set display type depending on users choice
  $header = ermmprofitability_getheaders($option);
  $query = ermmprofitability_getquery($option, $sort);

  // get data from table
  $queryResult = db_query($query);

  $count = 0;
  while ($links = db_fetch_object($queryResult)) {
    $rows[] = ermmprofitability_genrows($option, $links);
    $count++;
  }

  $table .= theme('table', $header, $rows ? $rows : array(array(array('data' => t('No data was returned.'), 'colspan' => 10))));
  if ($count > 1) $output .= '<p>'.$count.' '.t('records were returned.');
  $output .= theme('box', check_plain($title), $table);
  return $output;
} // function ermmprofitability_gettabledata

function ermm_profitability_formatrecord($option, $rowid) {

  switch ($option) {
    case 'job codes':
      $header = array('key' => array('data' => t('Viewing job code').' '.arg(3), 'colspan' => 2));
    break;

    case 'business units':
      $header = array('key' => array('data' => t('Viewing business unit').' '.arg(3), 'colspan' => 2));
    break;

    case 'customers':
      $header = array('key' => array('data' => t('Viewing customer').' '.arg(3), 'colspan' => 2));
    break;

    case 'groups':
      $header = array('key' => array('data' => t('Viewing group').' '.arg(3), 'colspan' => 2));
    break;
  }

  $links = ermmprofitability_getrowdata($option, $rowid);

  $rows = array();

  foreach ($links as $key => $val) {

    // rename the key from the database label to something more user-friendly
    $key = ermmprofitability_renamekeys($key);
    if ($key == 'Document link') $val = '<a href="'.$val.'">'.$val.'</a>';
    if (($key == 'Status') && ($option == 'job codes')) $val = ($val == '0') ? "Closed" : "Active";
    if (($key == 'Status') && ($option == 'customers')) $val = ($val == '1') ? "Prospective" : ((($val == '2') ? "Customer" : (($val == '3') ? "Partner"  : "Undefined")));


    $rows[] = array('key' => $key, 'val' => $val);
  }
  $table .= theme('table', $header, $rows ? $rows : array(array(array('data' => t('No data was returned.'), 'colspan' => 2))));
  $output .= theme('box', check_plain($title), $table);

  return $output;
} //ermm_profitability_formatrecord


function ermmprofitability_getoptions($option, $sort = 'none', $key, $optval) {

  $output = array();
  // build the query for this request
  $query = ermmprofitability_getquery($option, $sort, $rowid = NULL);

  // get data from table
  $queryResult = db_query($query);

  while ($links = db_fetch_object($queryResult)) {
    $select[$links->$key] = $links->$optval;
  }

  return $select;
} // function ermmprofitability_getoptions


function ermmprofitability_getrowdata($option, $rowid) {

  // build the query for this request
  $query = ermmprofitability_getquery($option, $sort = 'none', $rowid);

  // get data from table
  $queryResult = db_query($query);

  if (!$links = db_fetch_object($queryResult)) {
    $errormessage = 'No record with id of '.$rowid.' was found for '.$option;
    drupal_set_message($errormessage, error);
  }

  return $links;
} // function ermmprofitability_getrowdata


function ermmprofitability_renamekeys($key) {
  if ($key == 'jid') $key = 'Job code';
  if ($key == 'jname') $key = 'Name';
  if ($key == 'jdesc') $key = 'Description';
  if ($key == 'status') $key = 'Status';
  if ($key == 'buname') $key = 'Business unit';
  if ($key == 'custname') $key = 'Customer';
  if ($key == 'jgname') $key = 'Job group';
  if ($key == 'created') $key = 'Created on';
  if ($key == 'closed') $key = 'Closed on';
  if ($key == 'buid') $key = 'Business Unit Id';
  if ($key == 'custid') $key = 'Customer Id';
  if ($key == 'location') $key = 'Location';
  if ($key == 'jgid') $key = 'Id';
  if ($key == 'link') $key = 'Document link';

  return $key;
} //function ermmprofitability_renamekeys


function ermmprofitability_getform($option, $rowid, $mode) {
  if ($_POST['op'] == t('Delete')) {
    // Note: we redirect from /edit to /delete to make the tabs disappear.
    drupal_goto('ermm/profitability/'. arg(2).'/'. arg(3).'/delete');
  }
  switch ($option) {
    case 'job codes':
      if ($mode == 'edit') {
        $links = ermmprofitability_getrowdata('edit job codes', $rowid);
        $form['job codes']['id'] = array(
          '#type' => 'item',
          '#title' => t('Job code'),
          '#value' => $links->jid
        );
        $form['job codes']['jid'] = array(
          '#type' => 'hidden',
          '#title' => t('Job code'),
          '#value' => $links->jid
        );
      } else {
        $form['job codes']['jid'] = array(
          '#type'=> 'textfield',
          '#title' => t('Job code'),
          '#size' => '50',
          '#maxlength' => '5'
        );
      }
      $form['job codes']['jname'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->jname,
        '#title' => t('Name'),
        '#size' => '50',
        '#maxlength' => '50'
      );
      $form['job codes']['jdesc'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->jdesc,
        '#title' => t('Description'),
        '#size' => '50',
        '#maxlength' => '50'
      );
      $form['job codes']['status'] = array(
        '#type'=> 'select',
        '#default_value' => $links->status,
        '#width' => '50',
        '#title' => t('Status'),
        '#options' => array('1' => t('Active'), '2' => t('Closed'))
      );
      $form['job codes']['prevstatus'] = array(
        '#type' => 'hidden',
        '#title' => t('Previous status'),
        '#value' => $links->status
      );
      $businessunits = ermmprofitability_getoptions('business units', $sort = 'buname', 'buid', 'buname');
      $form['job codes']['buid'] = array(
        '#type'=> 'select',
        '#default_value' => $links->buid,
        '#title' => t('Business Unit'),
        '#options' => $businessunits
      );
      $customers = ermmprofitability_getoptions('customers', $sort = 'custname', 'custid', 'custname');
      $form['job codes']['custid'] = array(
        '#type'=> 'select',
        '#default_value' => $links->custid,
        '#title' => t('Customer'),
        '#options' => $customers
      );
      $groups = ermmprofitability_getoptions('groups', $sort = 'jgname', 'jgid', 'jgname');
      $form['job codes']['jgid'] = array(
        '#type'=> 'select',
        '#default_value' => $links->jgid,
        '#title' => t('Group'),
        '#options' => $groups
      );
      $form['job codes']['link'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->link,
        '#title' => t('Document link'),
        '#size' => '50',
        '#maxlength' => '254'
      );
      $form['job codes']['created'] = array(
        '#type' => 'item',
        '#title' => t('Created'),
        '#value' => $links->created
      );
      $form['job codes']['closed'] = array(
        '#type' => 'item',
        '#title' => t('Closed'),
        '#value' => $links->closed
      );
    break;

    case 'business units':
      if ($mode == 'edit') {
        $links = $links = ermmprofitability_getrowdata($option, $rowid);
        $form['business units']['id'] = array(
          '#type' => 'item',
          '#title' => t('Business Unit Id'),
          '#value' => $links->buid
        );
        $form['business units']['buid'] = array(
          '#type' => 'hidden',
          '#title' => t('Business Unit Id'),
          '#value' => $links->buid
        );
      }
      $form['business units']['buname'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->buname,
        '#title' => t('Name'),
        '#size' => '50',
        '#maxlength' => '50'
      );
    break;

    case 'customers':
      if ($mode == 'edit') {
        $links = $links = ermmprofitability_getrowdata($option, $rowid);
        $form['customers']['id'] = array(
          '#type' => 'item',
          '#title' => t('Customer Id'),
          '#value' => $links->custid
        );
        $form['customers']['custid'] = array(
          '#type' => 'hidden',
          '#title' => t('Customer Id'),
          '#value' => $links->custid
        );
      }
      $form['customers']['custname'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->custname,
        '#title' => t('Name'),
        '#size' => '50',
        '#maxlength' => '50'
      );
      $form['customers']['status'] = array(
        '#type'=> 'select',
        '#default_value' => $links->status,
        '#width' => '50',
        '#title' => t('Status'),
        '#options' => array('0' => t('Undefined'), '1' => t('Prospective'), '2' => t('Customer'), '3' => t('Partner')) 
      );
      $form['customers']['location'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->location,
        '#title' => t('Location'),
        '#size' => '50',
        '#maxlength' => '50'
      );
    break;

    case 'groups':
      if ($mode == 'edit') {
        $links = $links = ermmprofitability_getrowdata($option, $rowid);
        $form['groups']['id'] = array(
        '#type' => 'item',
          '#title' => t('Group Id'),
          '#value' => $links->jgid
        );
        $form['groups']['jgid'] = array(
          '#type' => 'hidden',
          '#title' => t('Group Id'),
          '#value' => $links->jgid
        );
      }
      if ($mode == 'new') {
        $form['groups']['jgid'] = array(
          '#type'=> 'textfield',
          '#default_value' => $links->jgname,
          '#title' => t('Job group Id'),
          '#size' => '50',
          '#maxlength' => '3'
        );
      }
      $form['groups']['jgname'] = array(
        '#type'=> 'textfield',
        '#default_value' => $links->jgname,
        '#title' => t('Name'),
        '#size' => '50',
        '#maxlength' => '50'
      );
    break;

  }

  $form['formname'] = array(
    '#type' => 'hidden',
    '#title' => t('Form name'),
    '#value' => $option
  );

  if ($mode == 'new') {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save'
    );
  }
  
  if ($mode == 'edit') {
    $form['delete'] = array(
      '#type' => 'submit',
      '#submit' => TRUE,
      '#value' => 'Delete'
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Update'
    );
  }

  $form[$option] += array(
    '#type' => 'fieldset',
    '#title' => ucfirst($mode).' '.$option,
    '#collapsible' => FALSE
  );

  return $form;
} //function ermmprofitability_getform


function ermmprofitability_delete_confirm($option, $rowid) {
  $form['groups']['id'] = array(
    '#type' => 'item',
    '#title' => t(''),
    '#value' => 'Are you sure you wish to delete this entry?'
  );

  $form['formname'] = array(
    '#type' => 'hidden',
    '#title' => t('Form name'),
    '#value' => $option
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#title' => t('Id'),
    '#value' => $rowid
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel'
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#submit' => TRUE,
    '#value' => 'Delete'
  );

  return $form;

} //function ermmprofitability_delete_confirm
