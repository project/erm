<?php

function ermmasset_getheaders($option) {
  switch ($option) {
    case 'assets':
      $header = array(t('ERM'), t('Part'), t('Serial'), t('Status'), t('Contact'));
    break;

  }

  return $header;

} // function ermmasset_getheaders


function ermmasset_getquery($option, $form_values, $rowid = NULL) {

  /* the option will either be 'asset' or 'assets', returning either a single row or possibly a collection of rows */

/* the initial code didn't check whether or not the user had entered values in any of the fields so the SELECT statement was always complex 
I've rewritten it to simplify the queries */
  $where = "WHERE";		
  $count = 0;

  switch ($option) {
    case 'assets':
      $query = "SELECT id, model, serial, astid, user_id " .
               "FROM {erm_asset} ";
      if (strlen($form_values['ermmasset_id']) > 0) {
        $where .= " (id like '%".$form_values['ermmasset_id']."%')";
        $count++;
      }
      if (strlen($form_values['ermmasset_serial']) > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (serial like '%".$form_values['ermmasset_serial']."%') ";
    }	
      if (strlen($form_values['ermmasset_ponumber']) > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (serial like '%".$form_values['ermmasset_ponumber']."%') ";
    }	
      if (strlen($form_values['ermmasset_acctinvnum']) > 0) {
        if ($count > 0) $where .= " AND";
        $where .= " (serial like '%".$form_values['ermmasset_acctinvnum']."%') ";
    }	

    /* if the user hasn't accessed the advanced form then we don't need to chack these values */
	        
/* 
      if (($form_values[ermmasset_acctinvdate]['month'] != '1') || ($form_values[ermmasset_acctinvdate]['day'] != '1') || ($form_values[ermmasset_acctinvdate]['year'] != '1900')) $query .= "AND (acctinvdate like '%".$form_values[ermmasset_acctinvdate]['year']."-".$form_values[ermmasset_acctinvdate]['month']."-".$form_values[ermmasset_acctinvdate]['day']."%') ";
      $query .= " AND (prjid like '%".$form_values['ermmasset_projectid']."%') " .
	       "AND (prjid like '%".$form_values['ermmasset_jid']."%') "; 
*/

               /*************
               * not yet supported
               *
	       *"AND (buid like '%".$form_values['ermmasset_buid']."%') " .
               *************/ 

/*	       "AND (countryid like '%".$form_values['ermmasset_country']."%') " .
	       "AND (rack like '%".$form_values['ermmasset_rackid']."%') " .
	       "AND (ipaddr like '%".$form_values['ermmasset_ip']."%') " .
	       "AND (user_id like '%".$form_values['ermmasset_userid']."%') ";
*/

               /*************
               * in-use check not yet supported
               *
	       * "AND (user_id like '%".$form_values['ermmasset_userid']."%') " .
               *************/ 
      break;
   
    case 'asset':
      $query = "SELECT * " .
               "FROM {erm_asset} " .
               "WHERE id = '".$rowid."' ";
    break;
  }
  if (strlen($where) > 6) $query .= $where;
  return $query;

} // function ermmasset_ermm_query;

/* formats a single row (with a link for the ERM) and the ERM ID prefixed by the letters "ERM" */
function ermmasset_genrows($option, $links) {
  switch ($option) {
    case 'assets':
      $row = array('ERM' => '<a href="'.arg(1).'/'.$links->id.'">ERM'.$links->id.'</a>', 'Part' => $links->model, 'Serial' => $links->serial, 'Status' => $links->astid, 'Contact' => $links->user_id);
    break;
  }

  return $row;

} //function ermmasset_genrows


function ermmasset_gettabledata($option, $form_values) {

  $output = '';

  $rows = array();

  // set display type depending on users choice
  $header = ermmasset_getheaders($option);
  $query = ermmasset_getquery($option, $form_values);

  // get data from table
  $queryResult = db_query($query);

  $count = 0;
  while ($links = db_fetch_object($queryResult)) {
    $rows[] = ermmasset_genrows($option, $links); 
    $count++;
  }

  $table .= theme('table', $header, $rows ? $rows : array(array(array('data' => t('No data was returned.'), 'colspan' => 10))));
  if ($count > 1) $output .= '<p>'.$count.' '.t('records were returned.');

  $output .= theme('box', check_plain($title), $table); 
  return $output;
} // function ermmasset_gettabledata

function ermm_asset_formatrecord($option, $rowid) {

  switch ($option) {
    case 'assets':
      $header = array('key' => array('data' => t('Viewing asset ERM').' '.arg(2), 'colspan' => 2));
    break;
  }

  $links = ermmasset_getrowdata($option, $rowid);

  $rows = array();

  foreach ($links as $key => $val) {

    // rename the key from the database label to something more user-friendly
    $key = ermmasset_renamekeys($key);
    if ($key == 'Document link') $val = '<a href="'.$val.'">'.$val.'</a>';
    if (($key == 'Status') && ($option == 'job codes')) $val = ($val == '0') ? "Closed" : "Active";
    if (($key == 'Status') && ($option == 'customers')) $val = ($val == '1') ? "Prospective" : ((($val == '2') ? "Customer" : (($val == '3') ? "Partner"  : "Undefined")));


    $rows[] = array('key' => $key, 'val' => $val);
  }
  $table .= theme('table', $header, $rows ? $rows : array(array(array('data' => t('No data was returned.'), 'colspan' => 2))));
  $output .= theme('box', check_plain($title), $table);

  return $output;
} //ermm_asset_formatrecord


function ermmasset_getoptions($option, $sort = 'none', $key, $optval) {

  $output = array();
  // build the query for this request
  $query = ermmasset_getquery($option, $sort, $rowid = NULL);

  // get data from table
  $queryResult = db_query($query);

  while ($links = db_fetch_object($queryResult)) {
    $select[$links->$key] = $links->$optval;
  }

  return $select;
} // function ermmasset_getoptions


function ermmasset_getrowdata($option, $rowid) {

  // build the query for this request
  $query = ermmasset_getquery($option, $sort = 'none', $rowid);

  // get data from table
  $queryResult = db_query($query);

  if (!$links = db_fetch_object($queryResult)) {
    $errormessage = 'No record with id of '.$rowid.' was found for '.$option;
    drupal_set_message($errormessage, error);
  }

  return $links;
} // function ermmasset_getrowdata


function ermmasset_renamekeys($key) {
  if ($key == 'jid') $key = 'Job code';
  if ($key == 'jname') $key = 'Name';
  if ($key == 'jdesc') $key = 'Description';
  if ($key == 'status') $key = 'Status';
  if ($key == 'buname') $key = 'Business unit';
  if ($key == 'custname') $key = 'Customer';
  if ($key == 'jgname') $key = 'Job group';
  if ($key == 'created') $key = 'Created on';
  if ($key == 'closed') $key = 'Closed on';
  if ($key == 'buid') $key = 'Business Unit Id';
  if ($key == 'custid') $key = 'Customer Id';
  if ($key == 'location') $key = 'Location';
  if ($key == 'jgid') $key = 'Id';
  if ($key == 'link') $key = 'Document link';

  return $key;
} //function ermmasset_renamekeys



function ermmasset_delete_confirm($option, $rowid) {
  $form['groups']['id'] = array(
    '#type' => 'item',
    '#title' => t(''),
    '#value' => 'Are you sure you wish to delete this entry?'
  );

  $form['formname'] = array(
    '#type' => 'hidden',
    '#title' => t('Form name'),
    '#value' => $option
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#title' => t('Id'),
    '#value' => $rowid
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel'
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#submit' => TRUE,
    '#value' => 'Delete'
  );

  return $form;

} //function ermmasset_delete_confirm
